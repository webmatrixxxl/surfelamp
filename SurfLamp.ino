#include <SoftwareSerial.h>
#define IP "thingspeak.com" // 184.106.153.149
#define setupTime  40000
#define Kabakum "EZ8VNNBVYUOFM7P0"
#define SunnyDay "1WKQZDC51YC5L9FV"
#define Obzor "UFFFTM3GCILUPSMI"

String PLACE = Kabakum;
String GET = "GET /apps/thinghttp/send_request?api_key=";

int RED = 9;
int GREEN = 10;
int BLUE = 11;

int maxConnectionAttempt = 10;
int connectionAttempt = 0;
int lastWaveColor;
bool isConnected = false;


#define dbg Serial  //For debugging purposes
SoftwareSerial esp(6, 7); //RX, TX


char ftUnit[] = "ft";
char mUnit[]  = "m";


float smallWaves = 0.7;
float mediumWaves = 1;
float hugeWaves = 1.5;

void setup()
{
  pinMode(RED, OUTPUT);
  pinMode(BLUE, OUTPUT);
  pinMode(GREEN, OUTPUT);

  digitalWrite(RED, LOW);
  digitalWrite(BLUE, LOW);
  digitalWrite(GREEN, LOW);

  BootOK();
  
  esp.begin(115200);
  esp.println("AT+CIOBAUD=9600");
  delay(1000);
  esp.end();
  delay(2000);

  esp.begin(9600);
  dbg.begin(9600); //debugger baud rate

  esp.println("AT\r\n");
  delay(1000);


  dbg.println("HELLO SURFER!\r\n");

  if ( esp.find("OK") ) {
    dbg.println("WIFI IS OK!");
    dbg.println("LET CHECK THE WAVES!");

    SetupMode();
    ConnectionStatus(3);    
   
  } else {
    dbg.println("WIFI IS BAD!");
    setColor(255, 0, 0); //red
  }
}

void loop()
{
  //Test Colors for waves
//  LowWave();
//  MiddleWave();
//  HighWave();
  
  delay(10000);

  if ( !CheckConnection(3) ) {

    connectionAttempt++;

    if (connectionAttempt >= maxConnectionAttempt) {
      InternetBad();
      connectionAttempt = 0;
    }
  }
  
  dbg.println();
  dbg.println("-----------------------------OK!");
  
  updateWave();
  
   esp.println("AT+CWMODE?");
  delay(2000);

  if ( !esp.find("+CWMODE:1") ) {
    esp.println("AT+CWMODE=1");

    dbg.println("SET MODE TO STATION.");
  }

}

void updateWave()
{
  dbg.println("REQUESTING THE WAVES HEIGHT...");
  
  String cmd = "AT+CIPSTART=\"TCP\",\"";
  cmd += IP;
  cmd += "\",80";

  esp.println(cmd);
  delay(2000);

  if ( esp.find("Error") ) {
    return;
  }

  cmd = GET;
  cmd += PLACE;
  cmd += "\r\n";
  esp.print("AT+CIPSEND=");
  esp.println(cmd.length());

  if ( esp.find(">") ) {
    esp.print(cmd);

    if ( esp.find("SEND OK") ) {
      delay(5000); // Wait for a minute site response
      
      if ( esp.find("IPD") ) {
        dbg.println("OK WE GOT WAVES HEIGHT!");
        
        String htmlResponce = esp.readString();
        int firstWave = htmlResponce.lastIndexOf(':');
        int secondWaveStart = htmlResponce.lastIndexOf('-');
        int secondWaveEnd = htmlResponce.indexOf('<');
        int unitStart = htmlResponce.indexOf(">");
        int unitEnd = htmlResponce.indexOf("</s");

        if ( secondWaveStart < 0 ) {
          secondWaveStart = firstWave;
          firstWave = -1;
        }

        String parsetWave1 = htmlResponce.substring(firstWave + 1, secondWaveStart);
        String parsetWave2 = htmlResponce.substring(secondWaveStart + 1, secondWaveEnd);

        parsetWave1.trim();
        parsetWave2.trim();

        float myWave1 = parsetWave1.toFloat();
        float myWave2 = parsetWave2.toFloat();
        String unit = htmlResponce.substring(unitStart + 1, unitEnd);
        
        //Print Parsed WAVES DATA
        dbg.println(myWave1);
        dbg.println(myWave2);
        dbg.println(htmlResponce); 

        if ( unit != mUnit ) {
          myWave1 *= 0.3048;
          myWave2 *= 0.3048;
          unit = mUnit;
        }

        dbg.print("WAVES: ");
        dbg.print(myWave1);
        dbg.print("-");
        dbg.print(myWave2);
        dbg.print(unit);
        dbg.print("\n");
   
        if ( ((myWave1  >= smallWaves) || (myWave2  >= smallWaves)) && ((myWave1 < mediumWaves) && (myWave2 < mediumWaves)) ) {
          LowWave();
        } else if ( ((myWave1  >= mediumWaves) || (myWave2  >= mediumWaves)) && ((myWave1 < hugeWaves) && (myWave2 < hugeWaves)) ) {
          MiddleWave();
        } else if ( (myWave1 >= hugeWaves) || (myWave2 >= hugeWaves) ) {
          HighWave();
        } else {
          SetDefaultColor();
        }
      }

    } else {
      dbg.println("No.. I don't found it!");
    }

  } else {
    esp.println("AT+CIPCLOSE");
  }
}

void setColor(int red, int green, int blue)
{
  analogWrite(RED, red);
  analogWrite(GREEN, green);
  analogWrite(BLUE, blue);
}

String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length() - 1;

  for ( int i = 0; i <= maxIndex && found <= index; i++ ) {
    if ( data.charAt(i) == separator || i == maxIndex ) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }

  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void controlledFade(int r1, int g1, int b1, int r2, int g2, int b2, int fadeTime)
{
  float rValue, gValue, bValue;
  float redDifference, blueDifference, greenDifference;
  int red1 = r1;
  int green1 = g1;
  int blue1 = b1;
  int red2 = r2;
  int green2 = g2;
  int blue2 = b2;

  redDifference = 255.0 / (red1 - red2);
  greenDifference = 255.0 / (green1 - green2);
  blueDifference = 255.0 / (blue1 - blue2);


  for ( int fadeValue = 0 ; fadeValue <= 255; fadeValue++ ) {
    rValue = red1 - fadeValue / redDifference;
    gValue = green1 - fadeValue / greenDifference;
    bValue = blue1 - fadeValue / blueDifference;

    analogWrite(RED, rValue);
    analogWrite(GREEN, gValue);
    analogWrite(BLUE, bValue);
    delay(fadeTime);
  }
}

void SetDefaultColor()
{
  lastWaveColor = 0;
  
  if ( isConnected ) {
    setColor(255, 255, 255);
  } else {
    setColor(255, 102, 102);
  }
}

void BootOK()
{
  controlledFade(0, 0, 0, 255, 255, 255, 10);
}

void SetupMode()
{
  dbg.print("WIFI SETUP MODE! WAIT: ");
  dbg.println(setupTime);

  esp.println("AT+CWSMARTSTART=0");

  unsigned long start = millis();
  controlledFade(255, 255, 255, 200, 20, 180, 1);

  while ( millis() - start <= setupTime ) {
   controlledFade(240, 20, 60, 200, 20, 180, 1);
   controlledFade(200, 20, 180, 100, 140, 180, 1);
   controlledFade(100, 140, 180, 240, 20, 60, 1);
  }
  
 
  controlledFade(240, 20, 60, 255, 255, 255, 1);
  
//  esp.println("AT+CWSMARTSTART=1");
}

void LastColor()
{
  switch(lastWaveColor) {
    case 0  :
      SetDefaultColor();
      break;
    case 1  :
      LowWave();
      break;
    case 2  :
      MiddleWave();
      break;
    case 3  :
      HighWave();
      break;
    default : /* Optional */ 
      break; /* optional */ 
  }
}

void ConnectionStatus(int attempts)
{
  if ( CheckConnection(attempts) ) {
    InternetOk();
  } else {
    InternetBad();
  }
}

bool CheckConnection(int attempts)
{
  for ( int i = 0; i < attempts; i++ ) {
    esp.flush();
    esp.println("AT+PING=\"8.8.8.8\"");
    delay(3000);
    
    if ( esp.find("ERROR") == false ) {
        isConnected = true;
        return true;
    }
  }
  
  isConnected = false;
  return false;
}

void InternetOk()
{  
  dbg.println("INTERNET IS OK!");
  unsigned long start = millis();
  
  while ( millis() - start <= 3000 ) {
    setColor(51, 51, 255);
    delay(100);
    setColor(0, 0, 0);
    delay(100);
    setColor(153, 153, 255);
  }
  
  LastColor();
}

void InternetBad()
{
  dbg.println("INTERNET TIMEOUT!");
  unsigned long start = millis();
  
  while ( millis() - start <= 3000 ) {
    setColor(255, 0, 0);
    delay(100);
    setColor(0, 0, 0);
    delay(100);
    setColor(220,20,60);
  }
  
  LastColor();
}

void LowWave()
{
  lastWaveColor = 1;
  controlledFade(5, 255, 226, 5, 159, 255, 30);
  controlledFade(5, 159, 255, 5, 255, 226, 30);
}

void MiddleWave()
{
  lastWaveColor = 2;
 controlledFade(0, 204, 102, 0, 255, 128, 30);
  controlledFade(0, 255, 128, 0, 204, 102, 30);
}

void HighWave()
{
  lastWaveColor = 3;
  controlledFade(240, 20, 60,158, 0, 179, 30);
  controlledFade(158, 0, 179, 240, 20, 60, 30);
}
